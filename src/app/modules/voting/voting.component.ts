import { candidates } from './interfaces/candidate';
import { voter } from './interfaces/voter';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-voting',
  templateUrl: './voting.component.html',
  styleUrls: ['./voting.component.scss']
})
export class VotingComponent implements OnInit {

  voters: voter[] = [];
  candidates: candidates[] = [];

  voterNameInput: string | null = null;
  candidateNameInput: string | null = null;

  voterSelectValue: string = '';
  candidateSelectValue: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  get voted(): boolean {
    return this.voters.find(x => x.name === this.voterSelectValue)?.voted ?? false;
  }

  get voterSelected(): boolean {
    return this.voterSelectValue !== '';
  }

  get candidateSelected(): boolean {
    return this.candidateSelectValue !== '';
  }

  addCandidate() {
    if (!this.candidateNameInput) {
      alert('Candidate name cannot be empty!');
    } else {
      if (this.candidates.some(x => x.name === this.candidateNameInput)) {
        alert('Candidate with given name exists!');
      } else {
        this.candidates.push({
          name: this.candidateNameInput,
          votes: 0,
        });
        this.candidateNameInput = null;
      }
    }
  }

  addVoter() {
    if (!this.voterNameInput) {
      alert('Voter name cannot be empty!');
    } else {
      if (this.voters.some(x => x.name === this.voterNameInput)) {
        alert('Voter with given name exists!');
      } else {
        this.voters.push({
          name: this.voterNameInput,
          voted: false,
        });
        this.voterNameInput = null;
      }
    }
  }

  vote() {
    if (!this.voterSelected || !this.candidateSelected) {
      alert('Candidate and voter need to be selected');
      return;
    }

    var voter = this.voters.find(x => x.name === this.voterSelectValue);
    var candidate = this.candidates.find(x => x.name === this.candidateSelectValue);
    if (
      voter && candidate
    ) {
      voter.voted = true;
      candidate.votes++;
    }
    this.voterSelectValue = '';
    this.candidateSelectValue = '';
  }
}
