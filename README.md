# Voting

## Preparation

### Clone repository:
###   git clone git@bitbucket.org:Maras0503/voting.git
### Open terminal in project location
### Run scripts:
###   npm install
###   npm start

## Development server

### To open application:
###   Open browser.
###   Navigate to `http://localhost:4200/`
